#!/bin/bash

DEFAULT_LENGTH=24
DEFAULT_TYPE=7

usage() {
    echo "Usage: $0 [length] [type]"
    echo "length: Length of the password (default: $DEFAULT_LENGTH)"
    echo "type: Type of characters to include (default: $DEFAULT_TYPE)"
    echo "1 - numbers"
    echo "2 - letters"
    echo "4 - special chars (like !, \", §, $, ...)"
    echo "8 - random unicode chars"
    exit 1
}

LENGTH=${1:-$DEFAULT_LENGTH}
TYPE=${2:-$DEFAULT_TYPE}

if ! [[ "$LENGTH" =~ ^[0-9]+$ ]] || [ "$LENGTH" -le 0 ]; then
    echo "Error: Length must be a positive integer."
    usage
fi

if ! [[ "$TYPE" =~ ^[0-9]+$ ]] || [ "$TYPE" -lt 1 ] || [ "$TYPE" -gt 15 ]; then
    echo "Error: Type must be a sum of the following values: 1, 2, 4, 8."
    usage
fi

NUMBERS="0123456789"
LETTERS="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
EXTRA="!\"§$%&'()*+,-./:;<=>?@[\\]^_{|}~"

CHAR_SET=""

if (( TYPE & 1 )); then
    CHAR_SET+="$NUMBERS"
fi
if (( TYPE & 2 )); then
    CHAR_SET+="$LETTERS"
fi
if (( TYPE & 4 )); then
    CHAR_SET+="$EXTRA"
fi
if (( TYPE & 8 )); then
    for (( i=0; i<LENGTH; i++ )); do
    	# TODO: make a list of valid unicode-blocks for special-char generation
        CODE_POINT=$(( RANDOM % 0xE0FF ))
        CHAR=$(printf "\\U$(printf '%08x' "$CODE_POINT")")
        CHAR_SET+="$CHAR"
    done
fi

if [ -z "$CHAR_SET" ]; then
    echo "Error: Character-set is empty."
    exit 1
fi

PASSWORD=$(cat /dev/urandom | tr -dc "$CHAR_SET" | fold -w "$LENGTH" | head -n 1)

echo -e "\"\e[30;47m$PASSWORD\e[0m\" copied to clipboard."
echo -n "$PASSWORD" | xclip -selection clipboard
