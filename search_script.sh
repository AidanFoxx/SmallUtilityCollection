#!/bin/bash

search_files() {
    local search_dir="$1"
    local search_string="$2"
    local recursive="$3"

    if [ "$recursive" = true ]; then
        results=$(grep -rl --exclude-dir={.git,node_modules} "$search_string" "$search_dir" 2>/dev/null)
    else
        results=$(grep -l "$search_string" "$search_dir"/* 2>/dev/null)
    fi

    if [ -z "$results" ]; then
        if [ "$recursive" = false ]; then
            echo "No matches found. Consider using the -r option for recursive search."
        else
            echo "No matches found."
        fi
    else
        echo "$results"
    fi
}

search_dir="."
recursive=false

if [ "$1" == "-r" ]; then
    recursive=true
    search_dir="$2"
elif [ -n "$1" ]; then
    search_dir="$1"
fi

echo "Searching in: $(realpath "$search_dir")"

while true; do
    read -p "Search for content: " content
    if [ -z "$content" ]; then
        echo "No content entered. Exiting..."
        break
    fi
    search_files "$search_dir" "$content" "$recursive"
done