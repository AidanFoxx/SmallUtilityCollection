#!/bin/bash

loremIpsum="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."

generate_lorem() {
    local length=$1
    local type=$2
    local output=""

    case $type in
        words)
            output=$(echo $loremIpsum | tr ' ' '\n' | head -n $length | tr '\n' ' ')
            ;;
        sentences)
            output=$(echo $loremIpsum | tr '.' '\n' | head -n $length | tr '\n' '.' | sed 's/\.$//')
            ;;
        paragraphs)
            for ((i=0; i<length; i++)); do
                output+="$loremIpsum\n\n"
            done
            ;;
        characters)
            output=$(echo -n $loremIpsum | head -c $length)
            ;;
        *)
            echo "Invalid type. Use 'words', 'sentences', 'paragraphs', or 'characters'."
            exit 1
            ;;
    esac

    echo -e "$output"
}

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <length> <type>"
    echo "Type: words | sentences | paragraphs | characters"
    exit 1
fi

length=$1
type=$2

if ! [[ "$length" =~ ^[0-9]+$ ]] || [ "$length" -le 0 ]; then
    echo "Length must be a positive integer."
    exit 1
fi

generate_lorem $length $type
